// "user strict";

// let createNewUser = function () {
// let newUser = {
//   firstName: prompt("Enter your first name"),
//   lastName: prompt("Enter your last name"),
//   getLogin: function() {
//     return `${this.firstName[0]}${this.lastName}`.toLowerCase()
//   },
// };
// return newUser;
// };
// console.log(createNewUser().getLogin());

// -  Задание 2.
// -
// -  Написать программу, которая будет приветствовать пользователя.
// -  Сперва пользователь вводит своё имя, после чего программа выводит в консоль
//    сообщение с учётом его должности.
// -
// -  Список должностей:
// -  Mike — CEO;
// -  Jane — CTO;
// -  Walter — программист:
// -  Oliver — менеджер;
// -  John — уборщик.
// -
// -  Если введёно не известное программе имя — вывести в консоль сообщение
//    «Пользователь не найден.».
// -
// -  Выполнить задачу в двух вариантах:
// -  -  используя конструкцию if/else if/else;
// -  -  используя конструкцию switch. \*/

// const name = prompt("Введите имя");
// switch (name) {
//     case "Mike":
//         alert("CEO");
//         break;
//     case "Jane":
//         alert("CTO");
//         break;
//     case "Walter":
//         alert("программист");
//         break;
//     case "Oliver":
//         alert("менеджер");
//         break;
//     case "John":
//         alert("уборщик");
//         break;
// };

let name = prompt ("Введите имя");

if(name == 'Mike'){
    alert('CEO');
} else if (name == 'Jane'){
    alert('CTO');
} else if (name == 'Walter'){
    alert('программист');
}